# frnumedical

## Project clone

```
git clone https://gitlab.com/genaro.munoz.obregon/test2nufront.git

cd test2nufront
```

## Project setup

```
npm install
```

### Setup .env file with

NODE_ENV=development
VUE_APP_API_URL=http://127.0.0.1:8000/api/
VUE_APP_LOGIN_REDIRECT=http://localhost:448/
VUE_BASE_URL=http://localhost:8080/

### Compiles and hot-reloads for development

```
npm run serve
```

### Abrir navegador

```
http://localhost:8080
```
