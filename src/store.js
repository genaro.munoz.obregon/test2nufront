import Vue from 'vue';
import Vuex from 'vuex';
import VuexPersistence from 'vuex-persist'

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        token: null
    },
    mutations: {
        setToken: (state, token) => {
            state.token = token;
        }
    },
    actions: {
        saveToken({ commit }, token) {
            commit('setToken', token);
        }
    },
    getters: {
        getToken(state) {
            return state.token;
        }
    },
    plugins: [new VuexPersistence().plugin]
});

export default store;
