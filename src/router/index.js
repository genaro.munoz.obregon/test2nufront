import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'
import LoginForm from '../components/LoginComponent.vue'
import RegisterForm from '../components/RegisterComponent.vue'
import SearchVINForm from '../components/SearchVINComponent.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/login',
    name: 'Login Page',
    component: LoginForm
  },
  {
    path: '/register',
    name: 'register',
    component: RegisterForm
  },
  {
    path: '/searchvin',
    name: 'Search VIN',
    component: SearchVINForm
  },
  {
    path: '/logout',
    name: 'Salir',
    component: SearchVINForm
  },
]

const router = new VueRouter({
  routes
})

export default router
