const { defineConfig } = require("@vue/cli-service");
module.exports = defineConfig({
  transpileDependencies: true,
  chainWebpack: config => {
    config.plugin('define').tap(args => {
      const env = args[0]['process.env']
      for (const key in process.env) {
        env[key] = JSON.stringify(process.env[key])
      }
      return args
    })
  }
});